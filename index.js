//import React from 'react'
//import ReactDOMServer from 'react-dom/server'
//import { StaticRouter } from 'react-router-dom'
const express = require('express')
const proxy = require('http-proxy-middleware') 
const cors = require('cors')

const app = express()

const PORT = 9000
app.use(cors())

app.use('/api', proxy({ target: 'https://cloud.1c.fitness/app02/199/hs/api/v1/', changeOrigin: true }))

//app.get('*', (req, res) => {
    //console.log("Request", req.url)
    //const context = {}
    //const markup = ReactDOMServer.renderToString(
        //<StaticRouter location={req.url} context={context}>
            //<Routes />
        //</StaticRouter>
    //)

    //// render the index template with the embedded React markup
    //let renderedPage = renderPage(markup)
    //return res.send(renderedPage)
//})


app.listen(PORT, err => {
  if (err) {
    console.error(err)
  } else {
    console.log(`ПОЕХАЛИ!!! на порту http://localhost:${PORT}`)
  }
})
